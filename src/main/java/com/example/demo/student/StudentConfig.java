/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.student;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author marcellin
 */
@Configuration
public class StudentConfig {
    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
    return args -> {
        Student naomy = new Student("Naomy Campbell", LocalDate.of(2000, Month.JULY, 1), "marcellin@wabo.work");
        Student koby = new Student("Koby O Bryan", LocalDate.of(2000, Month.JULY, 1), "black@mamba.cd");
        // Invoke the repository interface and save the
        // List of objects
        repository.saveAll(List.of(naomy, koby));        
    };
    }
    
}
